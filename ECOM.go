package main

import (
	"database/sql"
	"log"
	"net/http"
	"text/template"

	_ "github.com/go-sql-driver/mysql"
)

// DataTypes Used In Our Project
type User struct {
	Id      int
	Name    string
	City    string
	Number  int64
	Pincode int
}

// Main Funtion To Call And Create A Request And Connection With DataBase @MySQL

func dbConn() (db *sql.DB) { //SQL>Db Is Struct That Contains Entries To Get In DataBse
	dbDriver := "mysql"
	dbUser := "root"
	dbPass := "1234"
	dbName := "ecomdb"
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName) //ERR variable USe For Error , DB For Calling
	if err != nil {                                              //I Use Panic To get Error At Every Instance Of Running Time
		panic(err.Error())
	}
	return db
}

var tmpl = template.Must(template.ParseGlob("SuportFiles/*")) //Parse Globe Uses To Create A Template Ton Call In Pattarns

func Index(w http.ResponseWriter, r *http.Request) { // Responce And Request Common For all Intrstion With Web With GIN
	db := dbConn()
	selDB, err := db.Query("SELECT * FROM user ORDER BY id DESC") //SQL Query To Fetch Data From Database
	if err != nil {
		panic(err.Error())
	}
	user := User{}
	res := []User{} // [] Creating A Slice Of A User Table

	for selDB.Next() { //SELDB IS A VAriavble That Conatins Data For Printing
		var id int
		var name, city string
		var number int64
		var pincode int
		err = selDB.Scan(&id, &name, &city, &number, &pincode) //SELDB Used For SQLROW And Scan Funtion Covert Itno Common GO TYpes
		if err != nil {
			panic(err.Error())
		}
		user.Id = id
		user.Name = name
		user.City = city
		user.Number = number
		user.Pincode = pincode
		res = append(res, user)
	}
	tmpl.ExecuteTemplate(w, "Index", res) //Calling Of Template Thtat Is bin TMPL Formate
	defer db.Close()                      // We used Defer To Close It for Other Use if We Are Done
}

// Funtion To Show Individual User Acc To Unique ID Giving To That ...!!
//Show Template
func Show(w http.ResponseWriter, r *http.Request) {
	db := dbConn()
	nId := r.URL.Query().Get("id")
	selDB, err := db.Query("SELECT * FROM user WHERE id=?", nId)
	if err != nil {
		panic(err.Error())
	}
	user := User{}
	for selDB.Next() {
		var id int
		var name, city string
		var number int64
		var pincode int
		err = selDB.Scan(&id, &name, &city, &number, &pincode)
		if err != nil {
			panic(err.Error())
		}
		user.Id = id
		user.Name = name
		user.City = city
		user.Number = number
		user.Pincode = pincode
	}
	tmpl.ExecuteTemplate(w, "Show", user)
	defer db.Close()
}

func New(w http.ResponseWriter, r *http.Request) {
	tmpl.ExecuteTemplate(w, "New", nil)
}

func Edit(w http.ResponseWriter, r *http.Request) {
	db := dbConn()
	nId := r.URL.Query().Get("id")
	selDB, err := db.Query("SELECT * FROM user WHERE id=?", nId)
	if err != nil {
		panic(err.Error())
	}
	user := User{}
	for selDB.Next() {
		var id int
		var name, city string
		var number int64
		var pincode int
		err = selDB.Scan(&id, &name, &city, &number, &pincode)
		if err != nil {
			panic(err.Error())
		}
		user.Id = id
		user.Name = name
		user.City = city
		user.Number = number
		user.Pincode = pincode

	}
	tmpl.ExecuteTemplate(w, "Edit", user)
	defer db.Close()
}

func Insert(w http.ResponseWriter, r *http.Request) { // We Also Use A Funtion LOG PRINTLN To Directly Show The Transection In CMD
	db := dbConn()
	if r.Method == "POST" {
		name := r.FormValue("name")
		city := r.FormValue("city")
		number := r.FormValue("number")
		pincode := r.FormValue("pincode")
		insForm, err := db.Prepare("INSERT INTO user(name, city,number,pincode) VALUES(?,?,?,?)")
		if err != nil {
			panic(err.Error())
		}
		insForm.Exec(name, city, number, pincode) //Execute Prepared statement With Arguments
		log.Println("INSERT: Name: " + name + " | City: " + city + "| Number: " + number + "| Pincode" + pincode)
	}
	defer db.Close()
	http.Redirect(w, r, "/", 301)
}

func Update(w http.ResponseWriter, r *http.Request) {
	db := dbConn()
	if r.Method == "POST" {
		name := r.FormValue("name")
		city := r.FormValue("city")
		number := r.FormValue("number")
		pincode := r.FormValue("pincode")
		id := r.FormValue("uid")
		insForm, err := db.Prepare("UPDATE user SET name=?, city=? , number=?, pincode=? WHERE id=?")
		if err != nil {
			panic(err.Error())
		}
		insForm.Exec(name, city, number, pincode, id)
		log.Println("UPDATE: Name: " + name + " | City: " + city + "| Number: " + number + "| Pincode" + pincode)
	}
	defer db.Close()
	http.Redirect(w, r, "/", 301)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	db := dbConn()
	emp := r.URL.Query().Get("id")
	delForm, err := db.Prepare("DELETE FROM user WHERE id=?")
	if err != nil {
		panic(err.Error())
	}
	delForm.Exec(emp)
	log.Println("DELETE")
	defer db.Close()
	http.Redirect(w, r, "/", 301)
}

func main() {
	log.Println("Hey Buddy You Succefully Run Server ,Server Started on: http://localhost:3030")

	http.HandleFunc("/", Index) //These All Are The Functon  That Cobatins calling Of Derired Template That Work nTask
	http.HandleFunc("/show", Show)
	http.HandleFunc("/new", New)
	http.HandleFunc("/edit", Edit)
	http.HandleFunc("/insert", Insert)
	http.HandleFunc("/update", Update)
	http.HandleFunc("/delete", Delete)
	http.ListenAndServe(":3030", nil) // Main Server Set At Desire Address Open It to Browe The Project
}
